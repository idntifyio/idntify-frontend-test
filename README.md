![Idntify Logo](https://cdn.idntify.io/idntify-logo.png)

# Sr/Mid Frontend Developer Test

Idntify is launching a new product, as their project manager states, the frontend is more important than the backend for this new product, also, their clients are eager to try out their new platform. Your task is to build this production-grade platform for their users.

---

Idntify just launched their own bar and their own craft beer.
Their idea is to show the nearest competitors (other breweries). If the user visits a competitor after visiting Idntify's page, Idntify will get a small compensation for directing that customer to another brewery.
Their main concern is that their last users were commiting fraud too much and they think that by only accepting orders from authenticated users they will reduce these really high fraud levels. 

#### Platform
The new platform must:

1. Have a login page. This login requires a user (email or username) and a password.
2. Have a register page. The registration requires the user to enter their email, a password and a password confirmation.
3. Have a products page. The products shown come from a database. Each product has a fixed price (you choose it).
4. Have a cart page. The user can see the added products, add or substract 1 to the product quantity and delete added products.
5. Have a navbar on all pages where the user can see the current total ALWAYS
6. Redirect the user to the login page if the user lands on the products page and is not "authenticated", any form of alert with any message must appear letting the user know about this.
7. Have a logout button. 
8. Have a checkout page. In this checkout page the user must enter their information (phone, last name, name, billing address, etc). If the user continues with the checkout, a confirmation message is shown to the user and their cart is reset.
9. Have a competitors page. You can use the [following public API](https://www.openbrewerydb.org/)
10. Have a competitor's detail page.
11. Have a search bar on the competitors page. The user can search for a competitor by name (by using the API), the API call must be delayed at least 500ms after the last stroke on the search bar.

---

#### The whole solution should:
- Be portable, can be built and executed in any system
- Have a clear structure
- Be written in React for the frontend
- Be easy to grow with new functionality 
- Dont include binaries, .sh files, etc. Use a dependency management tool (npm or yarn).

---
#### Bonus Points For:
- Tests, the more coverage the better
- Comments, a good readme, instructions, etc.
- Docker images / CI
- Commit messages (include .git in zip)
- Clear scalability

#### Notes
- You can use any number of libraries you want, but the lighter your solution is, the better
- You can use any framework / library for building the UI (material-ui, tailwind, etc)
- You can any testing library you want (if you add tests)
- You can build the UI however you want. We want to see your creativity as well ;)
- Anywhere where a service might be needed, mock it. Include a timeout for the API response or something.
- The competitors page and the competitors detail page do not required the users to be authenticated
